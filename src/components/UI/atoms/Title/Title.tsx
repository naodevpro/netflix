import React from "react";
import styled from "styled-components";
import { Palette } from "../../../../styles/theme";

type TitleProps = {
  className?: string;
  id?: string;
  size: "small" | "medium" | "extralarge";
  weight?: "light" | "regular" | "medium";
  color?: Palette;
  style?: React.CSSProperties;
  children?: React.ReactNode;
};

const TitleStyled = styled("h1")`
  &.title--small {
    font-size: 1em;
    @media ${(props) => props.theme.breakpoints.tablet} {
      font-size: 1.3em;
    }
    @media ${(props) => props.theme.breakpoints.laptop} {
      font-size: 1.5em;
    }
  }
  &.title--medium {
    font-size: 1.9em;
    @media ${(props) => props.theme.breakpoints.tablet} {
      font-size: 2.4em;
    }
    @media ${(props) => props.theme.breakpoints.laptop} {
      font-size: 3em;
    }
  }
  &.title--extralarge {
    font-size: 2em;
    @media ${(props) => props.theme.breakpoints.tablet} {
      font-size: 3.3em;
    }
    @media ${(props) => props.theme.breakpoints.laptop} {
      font-size: 5em;
    }
  }
`;

const Title: React.FC<TitleProps> = ({
  className,
  id,
  size,
  weight,
  color,
  style,
  children,
}) => {
  const classNames = [
    `title--${size}`,
    weight && weight,
    color && `${color}`,
    className,
  ]
    .filter(Boolean)
    .join(" ");

  return (
    <TitleStyled
      id={id}
      data-testid="title-component"
      className={classNames}
      style={style}
    >
      {children}
    </TitleStyled>
  );
};

export default Title;
