import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";

import { theme } from "../../../../styles/theme";

import Title from "./Title";
import { ThemeProvider } from "styled-components";

describe("Title Component", () => {
  it("renders Title component with default props", () => {
    render(
      <ThemeProvider theme={theme}>
        <Title size="small" weight="regular" />
      </ThemeProvider>
    );

    const titleElement = screen.getByTestId("title-component");
    expect(titleElement).toBeDefined();
    expect(titleElement).toHaveClass("title--small");
    expect(titleElement).toHaveClass("regular");
  });

  it("renders Title component with custom props", () => {
    render(
      <ThemeProvider theme={theme}>
        <Title size="extralarge" weight="medium" color="white">
          Custom Title
        </Title>
      </ThemeProvider>
    );

    const titleElement = screen.getByTestId("title-component");

    expect(titleElement).toBeDefined();

    const classList = titleElement.classList;

    expect(classList.contains("title--extralarge")).toBe(true);
    expect(classList.contains("medium")).toBe(true);
    expect(classList.contains("white")).toBe(true);
    expect(titleElement).toHaveTextContent("Custom Title");
  });
});
