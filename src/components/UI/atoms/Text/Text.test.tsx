import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import { ThemeProvider } from "styled-components";

import Text from "./Text";

const mockTheme = {
  breakpoints: {
    tablet: "(min-width: 600px)",
    laptop: "(min-width: 1024px)",
  },
};

describe("Text Component", () => {
  it("renders Text component with default props", () => {
    render(
      <ThemeProvider theme={mockTheme}>
        <Text size="small" weight="regular" />
      </ThemeProvider>
    );

    const textElement = screen.getByTestId("text-component");
    expect(textElement).toBeDefined();
    expect(textElement).toHaveClass("text--small");
    expect(textElement).toHaveClass("regular");
  });

  it("renders Text component with custom props", () => {
    render(
      <ThemeProvider theme={mockTheme}>
        <Text size="extralarge" weight="medium" color="white">
          Custom Text
        </Text>
      </ThemeProvider>
    );

    const textElement = screen.getByTestId("text-component");

    expect(textElement).toBeDefined();

    const classList = textElement.classList;

    expect(classList.contains("text--extralarge")).toBe(true);
    expect(classList.contains("medium")).toBe(true);
    expect(classList.contains("white")).toBe(true);
    expect(textElement).toHaveTextContent("Custom Text");
  });
});
