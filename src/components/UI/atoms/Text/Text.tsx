import styled from "styled-components";
import { Palette } from "../../../../styles/theme";

type TextProps = {
  className?: string;
  id?: string;
  size: "small" | "extralarge" | "cta";
  weight: "light" | "regular" | "medium";
  color?: Palette;
  style?: React.CSSProperties;
  children?: React.ReactNode;
};

const TextStyled = styled("span")`
  &.text--small {
    font-size: 0.6em;
    @media ${(props) => props.theme.breakpoints.tablet} {
      font-size: 1em;
    }
    @media ${(props) => props.theme.breakpoints.laptop} {
      font-size: 1.3em;
    }
  }
  &.text--cta {
    font-size: 0.8em;
    @media ${(props) => props.theme.breakpoints.tablet} {
      font-size: 1.2em;
    }
    @media ${(props) => props.theme.breakpoints.laptop} {
      font-size: 1.4em;
    }
  }
`;

const Text: React.FC<TextProps> = ({
  className,
  id,
  size,
  weight,
  color,
  style,
  children,
}) => {
  const classNames = [
    `text--${size}`,
    weight && weight,
    color && `${color}`,
    className,
  ]
    .filter(Boolean)
    .join(" ");

  return (
    <TextStyled
      id={id}
      data-testid="text-component"
      className={classNames}
      style={style}
    >
      {children}
    </TextStyled>
  );
};

export default Text;
