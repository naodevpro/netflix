import styled from "styled-components";

const FlexBoxStyled = styled.div`
  &.flexbox {
    display: flex;
  }
  &.justify-flex-start {
    justify-content: flex-start;
  }
  &.justify-flex-end {
    justify-content: flex-end;
  }
  &.justify-center {
    justify-content: center;
  }
  &.justify-space-between {
    justify-content: space-between;
  }
  &.justify-space-around {
    justify-content: space-around;
  }
  &.justify-space-evenly {
    justify-content: space-evenly;
  }
  &.align-stretch {
    align-items: stretch;
  }
  &.align-flex-start {
    align-items: flex-start;
  }
  &.align-flex-end {
    align-items: flex-end;
  }
  &.align-center {
    align-items: center;
  }
  &.align-baseline {
    align-items: baseline;
  }
  &.flex-row {
    flex-direction: row;
  }
  &.row-reverse {
    flex-direction: row-reverse;
  }
  &.column {
    flex-direction: column;
  }
  &.column-reverse {
    flex-direction: column-reverse;
  }
  &.wrap {
    flex-wrap: wrap;
  }
  &.nowrap {
    flex-wrap: nowrap;
  }
  &.wrap-reverse {
    flex-wrap: wrap-reverse;
  }
`;

export default FlexBoxStyled;