import FlexBoxStyled from "./FlexboxStyle";

interface FlexBoxProps {
  height?: string;
  width?: string;
  className?: string;
  id?: string;
  flex?: "1" | "2" | "3" | "4" | "5";
  justifyContent?:
    | "flex-start"
    | "flex-end"
    | "center"
    | "space-between"
    | "space-around"
    | "space-evenly";
  alignItems?: "stretch" | "flex-start" | "flex-end" | "center" | "baseline";
  flexDirection?: "row" | "row-reverse" | "column" | "column-reverse";
  flexWrap?: "wrap" | "no-wrap" | "wrap-reverse";
  gap?: string;
  style?: React.CSSProperties;
  children?: React.ReactNode;
}

const FlexBox: React.FC<FlexBoxProps> = ({
  height,
  width,
  className,
  id,
  justifyContent,
  alignItems,
  flexDirection,
  gap,
  flexWrap,
  children,
  style,
}) => {
  const classes = `flexbox ${
    justifyContent ? `justify-${justifyContent} ` : ""
  }${alignItems ? `align-${alignItems} ` : ""}${
    flexDirection ? `${flexDirection} ` : ""
  }${flexWrap ? `${flexWrap} ` : ""}`;

  return (
    <FlexBoxStyled
      id={id}
      data-testid="flexbox-component"
      className={className ? `${className} ${classes}` : classes}
      style={{ width: width, height: height, gap: gap, ...style }}
    >
      {children}
    </FlexBoxStyled>
  );
};

export default FlexBox;
