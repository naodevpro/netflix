import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";

import FlexBox from "./FlexBox";

describe("FlexBox component", () => {
  it("renders FlexBox component with default props", () => {
    render(<FlexBox />);

    const flexBoxElement = screen.getByTestId("flexbox-component");
    const classList = flexBoxElement.classList;

    expect(classList.contains("flexbox")).toBe(true);
  });

  it("applies custom styles and classes based on props", () => {
    render(
      <FlexBox
        height="100px"
        width="200px"
        className="custom-class"
        justifyContent="center"
        alignItems="flex-start"
        flexDirection="column"
        flexWrap="wrap"
        gap="10px"
        style={{ backgroundColor: "red" }}
      >
        <div>Child</div>
      </FlexBox>
    );

    const flexBoxElement = screen.getByTestId("flexbox-component");

    expect(flexBoxElement).toBeDefined();

    const classList = flexBoxElement.classList;

    expect(classList.contains("flexbox")).toBe(true);
    expect(classList.contains("custom-class")).toBe(true);
    expect(flexBoxElement).toHaveStyle(
      "height: 100px; width: 200px; gap: 10px; background-color: red;"
    );
  });

  it("renders child elements", () => {
    render(
      <FlexBox>
        <div>Child Element 1</div>
        <div>Child Element 2</div>
      </FlexBox>
    );

    const childElementOne = screen.getByText("Child Element 1");
    const childElementTwo = screen.getByText("Child Element 2");
    expect(childElementOne).toBeInTheDocument();
    expect(childElementTwo).toBeInTheDocument();
  });
});
