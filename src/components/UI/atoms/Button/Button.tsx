import React from "react";
import classNames from "classnames";
import styled from "styled-components";

import arrowIcon from "../../../../assets/icons/arrowIcon.svg";
import infoIcon from "../../../../assets/icons/infoIcon.svg";

import Text from "../Text/Text";

type ButtonProps = {
  id?: string;
  className?: string;
  width?: string;
  variant: "default" | "transparent";
  onClick?: () => void;
  children?: React.ReactNode;
};

const ButtonStyled = styled("button")`
  height: 31px;
  min-width: 100px;
  display: flex;
  align-items: center;
  justify-content: center;
  gap: 10px;
  outline: none;
  border: none;
  border-radius: 0.3rem;
  padding: 1em;
  cursor: pointer;
  @media ${(props) => props.theme.breakpoints.tablet} {
    height: 45px;
    width: 120px;
  }
  @media ${(props) => props.theme.breakpoints.laptop} {
    height: 57px;
    width: 140px;
  }
  &.button--default {
    background-color: rgba(255, 255, 255, 0.75);
    &:hover {
      background-color: ${(props) => props.theme.colors.white};
    }
  }
  &.button--transparent {
    min-width: 130px;
    background-color: ${(props) => props.theme.colors.white30};
    @media ${(props) => props.theme.breakpoints.tablet} {
      width: 180px;
    }
    @media ${(props) => props.theme.breakpoints.laptop} {
      width: 220px;
    }
    &:hover {
      background-color: rgba(109, 109, 110, 0.4);
    }
  }
`;

const ArrowIconStyled = styled("img")`
  height: 18px;
`;

const Button: React.FC<ButtonProps> = ({
  id,
  className,
  children,
  variant,
  width,
  onClick,
}) => {
  const buttonClasses = classNames("button", {
    [`button--${variant}`]: variant,
  });

  const combinedClasses = classNames(buttonClasses, className);

  return (
    <ButtonStyled
      id={id}
      data-testid="button-component"
      className={combinedClasses}
      onClick={onClick}
      style={{ width: width }}
    >
      <ArrowIconStyled
        src={variant === "transparent" ? infoIcon : arrowIcon}
        alt="arrow-icon"
      />
      <Text
        size="cta"
        color={variant === "transparent" ? "white" : "black"}
        weight="medium"
      >
        {children}
      </Text>
    </ButtonStyled>
  );
};

export default Button;
