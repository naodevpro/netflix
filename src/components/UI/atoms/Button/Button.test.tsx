import { render, screen, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom";
import { ThemeProvider } from "styled-components";

import { theme } from "../../../../styles/theme";

import Button from "./Button";

// const mockTheme = {
//   breakpoints: {
//     tablet: "(min-width: 600px)",
//     laptop: "(min-width: 1024px)",
//   },
// };

describe("Button Component", () => {
  it("renders with default variant and onClick handler", () => {
    const mockOnClick = jest.fn();
    render(
      <ThemeProvider theme={theme}>
        <Button variant="default" onClick={mockOnClick}>
          Default Button
        </Button>
      </ThemeProvider>
    );

    const buttonElement = screen.getByTestId("button-component");
    expect(buttonElement).toBeDefined();
    expect(buttonElement).toHaveClass("button");
    expect(buttonElement).toHaveClass("button--default");

    fireEvent.click(buttonElement);
    expect(mockOnClick).toHaveBeenCalledTimes(1);
  });

  it("renders with transparent variant and custom width", () => {
    render(
      <ThemeProvider theme={theme}>
        <Button variant="transparent" width="200px">
          Transparent Button
        </Button>
      </ThemeProvider>
    );

    const buttonElement = screen.getByTestId("button-component");
    expect(buttonElement).toBeDefined();
    expect(buttonElement).toHaveClass("button");
    expect(buttonElement).toHaveClass("button--transparent");
    expect(buttonElement).toHaveStyle("width: 200px");
  });
});
