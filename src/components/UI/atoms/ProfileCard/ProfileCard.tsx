import styled from "styled-components";
import FlexBox from "../FlexBox/FlexBox";
import Text from "../Text/Text";

type ProfileProps = {
  avatar: string;
  username: string;
};

const ProfilePictureStyled = styled(FlexBox)<{ backgroundImage: string }>`
  height: 150px;
  width: 150px;
  background-color: white;
  background-image: url(${(props) => props.backgroundImage});
  background-size: cover;
  &:hover {
    border: 10px solid white;
    cursor: pointer;
  }
  @media ${(props) => props.theme.breakpoints.tablet} {
    height: 200px;
    width: 200px;
  }
`;

const ProfileCard: React.FunctionComponent<ProfileProps> = ({
  avatar,
  username,
}) => {
  return (
    <FlexBox
      data-testid="profile-card-component"
      flexDirection="column"
      alignItems="center"
      gap="20px"
    >
      <ProfilePictureStyled backgroundImage={avatar} />
      <Text size="small" color="gray" weight="regular">
        {username}
      </Text>
    </FlexBox>
  );
};
export default ProfileCard;
