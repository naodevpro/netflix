import styled from "styled-components";
import FlexBox from "../../atoms/FlexBox/FlexBox";
import Title from "../../atoms/Title/Title";
import { useEffect, useState } from "react";
import { MOVIES_TRENDINGS } from "../../../../constants/api";
import { fetchData } from "../../../../utils/useFetch";

export type MovieTrending = {
  adult: boolean;
  backdrop_path: string;
  first_air_date: string;
  genre_ids: [number];
  id: number;
  media_type: string;
  name: string;
  origin_country: [string];
  original_language: string;
  original_name: string;
  overview_name: string;
  overview: string;
  popularity: number;
  poster_path: string;
  vote_average: number;
  vote_count: number;
};

const MoviesTrendingsContainerStyled = styled(FlexBox)`
  overflow-x: scroll;
  overflow-y: hidden;
  margin-top: 0em;
`;

const MoviesContainerStyled = styled(FlexBox)`
  height: 100%;
`;

const MoviePosterStyled = styled(FlexBox)<{ backgroundImage: string }>`
  background-image: url(${(props) => props.backgroundImage});
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
  cursor: pointer;
  transition: 0.2s ease-in;
  &:hover {
    transform: scale(1.1);
  }
  @media ${(props) => props.theme.breakpoints.tablet} {
    min-height: 180px;
    min-width: 350px;
  }
  @media ${(props) => props.theme.breakpoints.laptop} {
    min-height: 200px;
  }
`;

const MoviesTrending: React.FunctionComponent = () => {
  const [moviesTrendings, setMoviesTrendings] = useState<MovieTrending[]>([]);

  const getMoviesTrendings = async () => {
    try {
      const data = await fetchData(MOVIES_TRENDINGS);

      if (data.results) {
        setMoviesTrendings(data.results);
      }
    } catch (error) {
      console.error(
        "Une erreur s'est produite lors de la récupération des données"
      );
    }
  };

  useEffect(() => {
    getMoviesTrendings();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <MoviesTrendingsContainerStyled
      className="p-1"
      flexDirection="column"
      height="100%"
      width="auto"
    >
      <Title className="mb-1" size="small" color="white" weight="medium">
        Films populaires
      </Title>
      <MoviesContainerStyled gap="5px">
        {moviesTrendings
          ? moviesTrendings.map((movie: MovieTrending) => {
              if (movie.backdrop_path) {
                return (
                  <FlexBox key={movie.id}>
                    <MoviePosterStyled
                      height="140px"
                      width="285px"
                      backgroundImage={`https://image.tmdb.org/t/p/original${movie?.backdrop_path}`}
                    >
                      {movie.name}
                    </MoviePosterStyled>
                  </FlexBox>
                );
              }
            })
          : null}
      </MoviesContainerStyled>
    </MoviesTrendingsContainerStyled>
  );
};
export default MoviesTrending;
