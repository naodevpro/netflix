import { useEffect, useState } from "react";
import styled from "styled-components";
import { NETFLIX_TV_SHOW } from "../../../../constants/api";
import { fetchData } from "../../../../utils/useFetch";
import FlexBox from "../../atoms/FlexBox/FlexBox";
import Title from "../../atoms/Title/Title";
import { Movie } from "../../organisms/Header/Header";

const NetflixContainerStyled = styled(FlexBox)`
  overflow-x: scroll;
  overflow-y: hidden;
  margin-top: 0em;
`;

const TvShowContainerStyled = styled(FlexBox)`
  height: 100%;
`;

const TvShowPosterStyled = styled(FlexBox)<{ backgroundImage: string }>`
  background-image: url(${(props) => props.backgroundImage});
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
  cursor: pointer;
  transition: 0.2s ease-in;
  &:hover {
    transform: scale(1.1);
  }
  @media ${(props) => props.theme.breakpoints.tablet} {
    min-height: 38px;
    min-width: 230px;
  }
  @media ${(props) => props.theme.breakpoints.laptop} {
    min-height: 400px;
    min-width: 285px;
  }
`;

const NetflixTvShow: React.FunctionComponent = () => {
  const [netflixTvShow, setNetflixTvShow] = useState<Movie[]>([]);

  const getNetflixTvShow = async () => {
    try {
      const data = await fetchData(NETFLIX_TV_SHOW);

      if (data.results) {
        setNetflixTvShow(data.results);
      }
    } catch (error) {
      console.error(
        "Une erreur s'est produite lors de la récupération des données"
      );
    }
  };

  useEffect(() => {
    getNetflixTvShow();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <NetflixContainerStyled
      className="p-1"
      flexDirection="column"
      height="100%"
      width="auto"
    >
      <Title className="mb-1" size="small" color="white" weight="medium">
        Netflix Populaire
      </Title>
      <TvShowContainerStyled gap="5px">
        {netflixTvShow
          ? netflixTvShow.map((tvShow) => {
              if (tvShow.backdrop_path) {
                return (
                  <FlexBox key={tvShow.id}>
                    <TvShowPosterStyled
                      height="300px"
                      width="185px"
                      backgroundImage={`https://image.tmdb.org/t/p/original${tvShow?.poster_path}`}
                    >
                      {tvShow.title}
                    </TvShowPosterStyled>
                  </FlexBox>
                );
              }
            })
          : null}
      </TvShowContainerStyled>
    </NetflixContainerStyled>
  );
};
export default NetflixTvShow;
