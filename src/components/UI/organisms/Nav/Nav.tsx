import styled from "styled-components";
import logo from "../../../../assets/logos/logo_nav.png";

const NavStyled = styled("nav")`
  background: rgba(0, 0, 0, 1);
  background: linear-gradient(
    180deg,
    rgba(0, 0, 0, 1) 0%,
    rgba(0, 0, 0, 0) 100%
  );
`;
const Nav: React.FunctionComponent = () => {
  return (
    <NavStyled className="p-1">
      <img src={logo} alt="logo" />
    </NavStyled>
  );
};

export default Nav;
