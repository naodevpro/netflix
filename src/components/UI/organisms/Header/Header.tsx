import { useEffect, useState } from "react";
import styled from "styled-components";

import { fetchData } from "../../../../utils/useFetch";
import { MOVIES_TOP_RATED } from "../../../../constants/api";

import FlexBox from "../../atoms/FlexBox/FlexBox";

import Title from "../../atoms/Title/Title";
import Text from "../../atoms/Text/Text";
import Button from "../../atoms/Button/Button";

import topRatedIcon from "../../../../assets/icons/topRatedIcon.png";

export type Movie = {
  adult: boolean;
  backdrop_path: string;
  genre_ids: [number];
  id: number;
  original_language: string;
  original_title: string;
  overview: string;
  popularity: number;
  poster_path: string;
  release_date: string;
  title: string;
  video: boolean;
  vote_average: number;
  vote_count: number;
};

const HeaderStyled = styled(FlexBox)<{ backgroundImage: string }>`
  position: relative;
  background-image: url(${(props) => props.backgroundImage});
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
  overflow: hidden;
  @media ${(props) => props.theme.breakpoints.tablet} {
    min-height: 100%;
    background-size: cover;
  }
  @media ${(props) => props.theme.breakpoints.laptop} {
    min-height: 80vh;
    background-size: cover;
    padding: 4em;
  }
`;

const TitleHeaderStyled = styled(Title)`
  @media ${(props) => props.theme.breakpoints.laptop} {
    word-wrap: normal;
    word-break: normal;
  }
`;

const GradientOverlayStyled = styled("div")`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background: linear-gradient(
    to right,
    rgba(0, 0, 0, 1),
    rgba(0, 0, 0, 0),
    rgba(0, 0, 0, 1)
  );
`;

const TopRatedIconStyled = styled("img")`
  height: 15px;
  @media ${(props) => props.theme.breakpoints.tablet} {
    height: 21px;
  }
  @media ${(props) => props.theme.breakpoints.laptop} {
    height: 30px;
    color: #ffffff37;
  }
`;

const HeaderContentStyled = styled(FlexBox)`
  position: relative;
  z-index: 2;
`;

const Header: React.FunctionComponent = () => {
  const [topRatedMovies, setTopRatedMovies] = useState<Movie[]>([]);

  const getTopRatedMovies = async () => {
    try {
      const data = await fetchData(MOVIES_TOP_RATED);
      if (data.results) {
        setTopRatedMovies(data.results);
      }
    } catch (error) {
      console.error(
        "Une erreur s'est produite lors de la récupération des données"
      );
    }
  };

  useEffect(() => {
    getTopRatedMovies();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <HeaderStyled
        className="header"
        flexDirection="column"
        justifyContent="center"
        height="400px"
        width="100vw"
        backgroundImage={`https://image.tmdb.org/t/p/original/${topRatedMovies[10]?.backdrop_path}`}
      >
        <GradientOverlayStyled />
        <HeaderContentStyled
          className="p-1"
          flexDirection="column"
          justifyContent="center"
          height="100%"
          width="50%"
          gap="5px"
        >
          <TitleHeaderStyled size="extralarge" color="white" weight="medium">
            {topRatedMovies[10]?.title}
          </TitleHeaderStyled>
          <FlexBox flexDirection="column" justifyContent="center">
            <FlexBox alignItems="center" gap="5px" height="100%">
              <TopRatedIconStyled
                className="top-rated-icon"
                src={topRatedIcon}
                alt="top rated icon"
              />
              <Title size="small" color="white" weight="medium">
                {topRatedMovies[10]?.original_title}
              </Title>
            </FlexBox>
            <Text className="mt-1" size="small" color="white" weight="light">
              {topRatedMovies[10]?.overview.slice(0, 300)}...
            </Text>
          </FlexBox>
          <FlexBox alignItems="center" gap="20px">
            <Button className="mt-1" variant="default">
              Lecture
            </Button>
            <Button className="mt-1" variant="transparent">
              En savoir plus
            </Button>
          </FlexBox>
        </HeaderContentStyled>
      </HeaderStyled>
    </>
  );
};
export default Header;
