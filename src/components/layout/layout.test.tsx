import { render } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";
import Layout from "./layout";

test("renders Layout component", () => {
  render(
    <MemoryRouter>
      <Layout />
    </MemoryRouter>
  );
});
