import { Outlet } from "react-router-dom";
import styled from "styled-components";
import FlexBox from "../UI/atoms/FlexBox/FlexBox";
import Nav from "../UI/organisms/Nav/Nav";

const LayoutStyled = styled(FlexBox)`
  min-height: 100vh;
  background-image: url("../../assets/illustrations/background-blue.png");
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
`;

const Layout: React.FunctionComponent = () => {
  return (
    <LayoutStyled flexDirection="column">
      <FlexBox flexDirection="column">
        <Nav />
        <main>
          <Outlet />
        </main>
      </FlexBox>
    </LayoutStyled>
  );
};

export default Layout;
