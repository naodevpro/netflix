import FlexBox from "../../UI/atoms/FlexBox/FlexBox";
import MoviesTrending from "../../UI/molecules/MoviesTrendings/MoviesTrendings";
import NetflixTvShow from "../../UI/molecules/NetflixTvShow/NetflixTvShow";
import Header from "../../UI/organisms/Header/Header";

const Browse = () => {
  return (
    <FlexBox flexDirection="column">
      <Header />
      <NetflixTvShow />
      <MoviesTrending />
    </FlexBox>
  );
};

export default Browse;
