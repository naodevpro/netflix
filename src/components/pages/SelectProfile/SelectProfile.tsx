import { Link } from "react-router-dom";

import FlexBox from "../../UI/atoms/FlexBox/FlexBox";
import Title from "../../UI/atoms/Title/Title";
import ProfileCard from "../../UI/atoms/ProfileCard/ProfileCard";

import profilePicture from "../../../assets/illustrations/profilesCards/ProfilePicture-2.png";

const SelectProfile: React.FunctionComponent = () => {
  return (
    <FlexBox
      id="container-select-profile"
      flexDirection="column"
      justifyContent="center"
      alignItems="center"
      gap="50px"
      height="90vh"
    >
      <Title size="medium" weight="light" color="white">
        Qui est-ce ?
      </Title>
      <FlexBox gap="50px" alignItems="center">
        <Link to={"browse/stan"}>
          <ProfileCard avatar={profilePicture} username="Stan" />
        </Link>
      </FlexBox>
    </FlexBox>
  );
};
export default SelectProfile;
