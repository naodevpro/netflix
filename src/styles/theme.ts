export const theme: Theme = {
    colors: {
      white: '#ffffff',
      white30: 'rgba(109, 109, 110, 0.7);',
      smokeWhite: '#E5E5E5',
      midGray: '#6D6D6E',
      gray: '#808080',
      black: '#141414',
      red: '#B9090B',
    },
    typography: {
        light: 'NetflixSans-Light',
        regular: 'NetflixSans-Regular',
        medium: 'NetflixSans-Medium'
    },
    breakpoints: {
        mobile: '(min-width: 600px)',
        tablet: '(min-width: 768px)',
        laptop: '(min-width: 1024px)',
        desktop: '(min-width: 1280px)',
        xlarge : '(min-width: 1400px)',
    },
};

export type Theme = {
    colors: Color;
    typography: Typography;
    breakpoints: Breakpoint;
}   

export type Palette = "white" | "whiteOpacity" | "smokeWhite" | "midGray" | "gray" | "black" | "red";

export interface Color {
    white: string;
    white30: string;
    smokeWhite: string;
    midGray: string;
    gray: string;
    black: string;
    red: string;
}

export interface Typography {
    light: string;
    regular: string;
    medium: string;
} 

export interface Breakpoint {
    mobile: string;
    tablet: string;
    laptop: string;
    desktop: string;
    xlarge: string;
}

export default theme;