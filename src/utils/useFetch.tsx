import axios from "axios";

export const fetchData = async (url: string) => {
  try {
    const response = await axios.get(url);
    return response.data;
  } catch (error) {
    console.error(
      "Une erreur s'est produite lors de la récupération des données"
    );
  }
};
