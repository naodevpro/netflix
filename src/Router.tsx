import { Routes, Route } from "react-router-dom";
import Layout from "./components/layout/layout";
import SelectProfile from "./components/pages/SelectProfile/SelectProfile";
import Browse from "./components/pages/Browse/Browse";

const Router: React.FunctionComponent = () => {
  return (
    <Routes>
      <Route path="/" element={<Layout />}>
        <Route index element={<SelectProfile />} />
        <Route path="/browse/:username" element={<Browse />} />
        {/* <Route path="*" element={<NoMatch />} /> */}
      </Route>
    </Routes>
  );
};
export default Router;
