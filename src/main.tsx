import React from "react";
import ReactDOM from "react-dom/client";
import { BrowserRouter } from "react-router-dom";
import { ThemeProvider } from "styled-components";
import Router from "./Router.tsx";
import GlobalStyles from "./styles/GlobalStyles.ts";
import theme from "./styles/theme.ts";

ReactDOM.createRoot(document.getElementById("root")!).render(
  <ThemeProvider theme={theme}>
    <React.StrictMode>
      <BrowserRouter>
        <GlobalStyles />
        <Router />
      </BrowserRouter>
    </React.StrictMode>
  </ThemeProvider>
);
