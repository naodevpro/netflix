import { render } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";
import Router from "./Router";

import { theme } from "./styles/theme";
import { ThemeProvider } from "styled-components";

test("renders Router component", () => {
  render(
    <ThemeProvider theme={theme}>
      <MemoryRouter initialEntries={["/"]}>
        <Router />
      </MemoryRouter>
    </ThemeProvider>
  );
});
