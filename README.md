# 🚀 Netflix Clone with TypeScript, React, ViteJS, and Styled Components

A clone of the Netflix user interface built with TypeScript, React, ViteJS, and Styled Components.

## Table of Contents

- [Introduction](#introduction)
- [Features](#features)
- [Tech Stack](#tech-stack)
- [Getting Started](#getting-started)
- [Conventions Naming](#convention-naming)
## Introduction

Welcome to the Netflix Clone project! This project aims to recreate the user interface of Netflix, providing a modern and responsive design for users to explore and discover movies and TV shows.

## Features

- Browse a curated list of movies and TV shows.
- View detailed information about each title.
- Responsive design for an optimal viewing experience on various devices.

## Tech Stack

- TypeScript
- React
- ViteJS
- Styled Components
- Personalized hooks (for localStorage usage)
- useContext
- Sass

## Getting Started

To get started with the project locally, follow these steps:

```bash
# Clone the repository
git clone https://gitlab.com/naoxee/netflix-clone.git

# Change into the project directory
cd netflix

# Install dependencies
npm install

# Run project on local environment
npm run dev

# Run tests on local environment
npm run test
```

## Convention Naming

### Branch Naming Conventions:

- **`config/`**: This prefix is used for branches that involve changes to project configurations. It helps in quickly identifying branches that deal with build tools, linters, or other project settings.

- **`rebase/`**: The `rebase/` prefix indicates branches created explicitly for rebasing purposes. When working on complex features or changes, using a separate branch for rebasing helps keep the main branch history clean and linear.

- **`fix/`**: The `fix/` prefix is crucial for branches dedicated to resolving bugs or issues. It streamlines the process of identifying branches aimed at fixing problems, making it easier to track bug resolutions.

- **`hotfix/`**: For critical bug fixes that require immediate attention, the `hotfix/` prefix is employed. These branches help prioritize and swiftly address urgent issues without disrupting ongoing development.

- **`feat/`**: The `feat/` prefix signifies branches introducing new features or enhancements. This convention assists in quickly recognizing branches that focus on expanding or improving the application's functionality.

### Commit Naming Conventions:

- **`config:`**: Commits with the `config:` prefix are related to changes in project configurations. This helps in distinguishing commits specifically addressing project setup or configuration adjustments.

- **`rebase:`**: Commits with the `rebase:` prefix are related to actions involving the rebase process. This convention provides clarity on commits that are part of the rebase workflow.

- **`fix:`**: Commits prefixed with `fix:` are dedicated to resolving bugs or issues. This convention aids in identifying commits targeted at bug fixes when reviewing the version history.

- **`hotfix:`**: Commits with the `hotfix:` prefix are aimed at critical bug fixes requiring immediate attention. This prefix assists in quickly recognizing commits that address urgent issues.

- **`feat:`**: Commits with the `feat:` prefix focus on introducing new features or enhancements. This convention makes it straightforward to identify commits contributing to the addition of functionality.

Using these conventions not only brings consistency to the version history but also enhances collaboration among team members. It provides a clear structure, making it easier to navigate through branches and commits, ultimately streamlining the development workflow.


